# docker build -t oba --no-cache .
# docker run --name oba -p 9998:9998 -v /mnt/c/Users/kevin/OneDrive/Desktop/GIT/oba/srv/ontologies/:/srv/ontologies/ oba
# /srv/ontologies/ has to contain the ontologie AND a properties file, otherwise it wont appear

FROM maven:3.8.1-jdk-11 AS build
WORKDIR /work/
COPY . /work/
RUN cd /work
RUN mvn clean install

FROM openjdk:11-jre-slim
WORKDIR /work/

COPY --from=build /work/ /work/
RUN chmod 775 /work
RUN chmod +x /work

CMD ["java", "-jar", "/work/oba-server/target/oba-server-1.4-SNAPSHOT.jar"]